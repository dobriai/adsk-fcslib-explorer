// import logo from './logo.svg';
import './App.css';
import * as React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  useLocation,
} from "react-router-dom";
import {
  Stack, List, TextField, IconButton, Pagination,
  RadioGroup, FormControlLabel, Radio,
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { blueGrey } from '@mui/material/colors';
import LibListItem from './comps/LibListItem';

function appendParam(curr, paramName, paramVal) {
  if (!paramVal) {
    return curr;
  }
  return curr + (curr ? '&' : '?') + paramName + '=' + paramVal;
}

const styles = {
  Radio: { '& .MuiSvgIcon-root': { fontSize: 10, }, },
};

const darkTheme = createTheme({ // Use React.useMemo() inside component if ever switching themes! Locally.
  palette: {
    mode: 'dark',
    background: {
      default: blueGrey[900],
      paper: blueGrey[900],
    },
  },
});

function AppHome(props) {
  const [tags, setTags] = React.useState('');
  const [createdByAppId, setCreatedByAppId] = React.useState('');
  const [page, setPage] = React.useState(0);
  const [numPages, setNumPages] = React.useState(0);
  const [envir, setEnvir] = React.useState('prd');
  const { libData, setLibData } = props;

  const urlBase = {
    'prd': 'https://developer.api.autodesk.com/content/v1/libraries',
    'stg': 'https://developer-stg.api.autodesk.com/content/v1/libraries',
  };
  const filterBase = 'filter[contents]';
  const doSearch = async (pg) => {
    let params = '';
    params = appendParam(params, 'page[number]', pg);
    params = appendParam(params, filterBase + '[tags]', tags);
    params = appendParam(params, filterBase + '[createdByAppId]', createdByAppId);
    const url = urlBase[envir] + params;
    const resp = await fetch(url);
    const json = await resp.json();
    const { meta: { paginationInfo: { totalPages, pageNumber } } } = json;
    setNumPages(1 * totalPages);
    setPage(1 * pageNumber);
    setLibData(json);
  };

  const onTags = (ee) => {
    setTags(ee.target.value);
    setPage(0);
    setNumPages(0);
  };
  const onAppId = (ee) => {
    setCreatedByAppId(ee.target.value);
    setPage(0);
    setNumPages(0);
  };
  const onSearch = () => {
    doSearch(1)
  }
  const onPage = (ee, pg) => {
    doSearch(pg);
  };
  const onEnvir = (ee) => {
    setEnvir(ee.target.value);
  };

  return (
    <div className="App container">
      <header>
        <Stack spacing={2} direction="row">
          <RadioGroup value={envir} onChange={onEnvir}>
            <FormControlLabel value="prd" control={<Radio sx={styles.Radio} />} label="PRD" />
            <FormControlLabel value="stg" control={<Radio sx={styles.Radio} />} label="STG" />
          </RadioGroup>
          <TextField
            variant='outlined' fullWidth label="Library tag(s)"
            helperText="Optional. Comma separated." value={tags} onChange={onTags}
          />
          <TextField
            variant='outlined' fullWidth label="Creator App Id"
            helperText="Optional. Your FCS app id." value={createdByAppId} onChange={onAppId}
          />
          <div>
            <IconButton variant="contained" onClick={onSearch}><SearchIcon fontSize="large" /></IconButton>
          </div>
        </Stack>
      </header>
      <div className='body'>
        <List dense={false} disablePadding={true}>
          <LibListItem libData={libData} />
        </List>
      </div>
      {(numPages > 0) && (
        <footer>
          <Stack direction="row" justifyContent="center">
            <Pagination count={numPages} page={page} showFirstButton showLastButton onChange={onPage} />
          </Stack>
        </footer>
      )}
    </div >
  );
};

const Blahh = () => (<p>123 - blahh: Detailed lib info should come here!</p>);

const DebugRouter = ({ children }) => {
  const location = useLocation();
  console.log(
    `Route: ${location.pathname}${location.search}, State: ${JSON.stringify(
      location.state,
    )}`
  )
  return children
}

function App() {
  const [libData, setLibData] = React.useState({});
  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <DebugRouter>
          <Routes>
            <Route path="/" element={<AppHome libData={libData} setLibData={setLibData} />} />
            <Route path="/123" element={<Blahh />} />
          </Routes>
        </DebugRouter>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;
