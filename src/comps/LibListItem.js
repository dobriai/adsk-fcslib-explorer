import * as React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
  ListItem, ListItemText, IconButton, Button, Stack, Typography,
} from '@mui/material';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const styles = {
  ListItem: { "paddingTop": "2px", "paddingBottom": "2px", },
  ListItemText: { "margin": "0", },
  ButtonEC: { "paddingTop": "0", "paddingBottom": "0", }
};

const ButtonExpand = (props) => (
  <Button sx={styles.ButtonEC} size="small" onClick={props.onClick}>
    {props.expanded ? 'Collapse' : 'Expand'}
  </Button>
);

const LibDetails = (props) => {
  const [expanded, setExpanded] = React.useState(false);
  const { libItem } = props;
  return (
    <Typography variant="body2" sx={{ color: 'text.secondary' }} component="div">
      {expanded
        ? <Stack spacing={0.5}>
          <ButtonExpand expanded={expanded} onClick={() => setExpanded(!expanded)} />
          <pre>{JSON.stringify(libItem, null, 2)}</pre>
        </Stack>
        : <Stack direction="row">
          {libItem.attributes.createTime}, {libItem.id}, {libItem.attributes.description}
          &nbsp;
          <ButtonExpand expanded={expanded} onClick={() => setExpanded(!expanded)} />
        </Stack>
      }
    </Typography>
  );
};

const LibListItem = (props) => {
  const { libData } = props;
  return (
    libData && Array.isArray(libData.data) && libData.data.map(xx => (
      <ListItem key={xx.id} sx={styles.ListItem}>
        <Stack sx={{ flex: "1 1" }}>
          <ListItemText sx={styles.ListItemText} primary={`${xx.attributes.title}`} />
          <LibDetails libItem={xx} />
        </Stack>
        <IconButton component={RouterLink} to="123"><ArrowForwardIosIcon fontSize="medium" /></IconButton>
      </ListItem>))
  );
};
export default LibListItem;
